FROM registry.gitlab.com/avisi/base/java:8-jdk
EXPOSE 8080

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ENV WILDFLY_VERSION 16.0.0.Final
ENV WILDFLY_SHA1 287c21b069ec6ecd80472afec01384093ed8eb7d
ENV WILDFLY_HOME /opt/avisi/wildfly

ENV MANAGEMENT_INTERFACE=127.0.0.1

USER root

# Add the WildFly distribution to /opt, and make wildfly the owner of the extracted tar content
# Make sure the distribution is available from a well-known place
RUN curl -O "https://download.jboss.org/wildfly/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz" \
    && sha1sum wildfly-$WILDFLY_VERSION.tar.gz | grep $WILDFLY_SHA1 \
    && tar xf wildfly-$WILDFLY_VERSION.tar.gz \
    && mv wildfly-$WILDFLY_VERSION $WILDFLY_HOME \
    && rm wildfly-$WILDFLY_VERSION.tar.gz \
    && chown -R avisi:0 ${WILDFLY_HOME} \
    && chmod -R g+rw ${WILDFLY_HOME}

USER avisi

COPY entrypoint.sh /usr/local/bin/docker-entrypoint
COPY standalone.sh /opt/avisi/wildfly/bin/standalone.sh

ENTRYPOINT [ "/usr/local/bin/tini", "--" ]
CMD ["docker-entrypoint"]
