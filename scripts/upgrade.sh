#!/bin/bash


get_latest_release() {
  curl --silent "https://api.github.com/repos/$1/tags?per_page=1" | # Get latest release from GitHub api
    grep '"name":' |                                            # Get tag line
    sed -E 's/.*"([^"]+)".*/\1/'                                    # Pluck JSON value
}

LATEST_RELEASE=$(get_latest_release "wildfly/wildfly")

sed "s/ENV WILDFLY_VERSION.*/ENV WILDFLY_VERSION ${LATEST_RELEASE}/g" Dockerfile > Dockerfile.tmp
rm Dockerfile && mv Dockerfile.tmp Dockerfile

WILDFLY_SHA1=$(curl --silent "https://repo1.maven.org/maven2/org/wildfly/wildfly-dist/${LATEST_RELEASE}/wildfly-dist-${LATEST_RELEASE}.tar.gz.sha1")

sed "s/ENV WILDFLY_SHA1.*/ENV WILDFLY_SHA1 ${WILDFLY_SHA1}/g" Dockerfile > Dockerfile.tmp
rm Dockerfile && mv Dockerfile.tmp Dockerfile
