#  Docker wildfly

This repository contains the Avisi PEC Wildfly Docker base image.

[![pipeline status](https://gitlab.com/avisi/pec/wildfly/badges/master/pipeline.svg)](https://gitlab.com/avisi/pec/wildfly/commits/master)

## Table of Contents

- [Usage](#usage)
- [Automated build](#automated-build)
- [Contribute](#contribute)
- [License](#license)

## Usage

- TODO

### Dockerfile

```dockerfile
FROM registry.gitlab.com/avisi/pec/wildfly:latest
COPY myapplication.war application.war
```

The container also takes `JAVA_OPTS` and passes those to the jvm.

## Automated build

This image is build at least once a week automatically. All PR's are automatically build.

## Contribute

PRs accepted. All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/pec/wildfly/issues).

## License

[MIT © Avisi B.V.](LICENSE)
