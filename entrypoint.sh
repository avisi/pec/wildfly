#!/bin/bash
set -e

PORT_OFFSET=${PORT_OFFSET:-0}

# First clean up old signal files
rm -rf $WILDFLY_HOME/standalone/deployments/*.deployed
rm -rf $WILDFLY_HOME/standalone/deployments/*.failed

# start wildfly
exec /opt/avisi/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement $MANAGEMENT_INTERFACE -Djboss.socket.binding.port-offset=$PORT_OFFSET
